# Sample Label Production Project

## Purpose

This project is a representation of how NetDespatch use the peernet labelling libraries to produce labels. It includes a simple web page that demonstrates the embedding of the peernet applet.

## Build and Run Locally

### Define properties required
Create a custom yml properties file with the following configuration

    serialNumber:

    applicationRootDir: src/dist/appRoot

    http:
        rootPath: /api/*

### Start a local instance
To run a local instance, run the main class LabelTestServer by setting the following vm args

    server src/test/test_peernetTestProjectApplication.yml

## Distribute

Package the product using

    ./gradlew clean distZip

Add the following parameter to the above command in order to override the default peernet version (3.0.079)

    -PpeernetVersion=3.0.091

The following are the versions of Peernet that can be used with this project

    -PpeernetVersion=2.6.0
    -PpeernetVersion=3.0.100

### Start a distributed version

The zip created will contain all the resources required to run the project. Unzip the distribution from build/distributions to the location of your choice and then run the following command, you will then need to update the serial number in the yml properties file:

    ./bin/PeernetTestProject server peernetTestProjectApplication.yml

### Navigate to the application
Navigate to port 8080 on machine the server has been started on.

    e.g. http://localhost:8080/index.html

### Use resources directly

        GET     http://localhost:8080/api/label/applet
        GET     http://localhost:8080/api/label/pdf
        GET     http://localhost:8080/api/label/png

