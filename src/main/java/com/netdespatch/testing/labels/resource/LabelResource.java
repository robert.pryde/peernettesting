package com.netdespatch.testing.labels.resource;

import com.netdespatch.testing.labels.printing.peernet.LabelContentType;
import com.netdespatch.testing.labels.printing.peernet.PeernetException;
import com.netdespatch.testing.labels.service.LabelService;
import com.netdespatch.testing.labels.service.LabelServiceImpl;
import com.netdespatch.testing.labels.utils.ZipException;
import com.yammer.metrics.annotation.Timed;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.UUID;

/**
 * @author rpryde
 * @since 14/10/2015
 */
@Path("/label")
public class LabelResource {
    private static final Logger logger = LoggerFactory.getLogger(LabelServiceImpl.class);

    private final LabelService labelService;

    public LabelResource(String applicationRootDir, String serialNumber) {
        labelService = new LabelServiceImpl(applicationRootDir, serialNumber);
    }

    @GET
    @Timed
    @Path("/pdf")
    @Produces({"application/pdf"})
    public StreamingOutput getPDF(@Context final HttpServletResponse response) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                FileInputStream inputStream = null;
                try {
                    File file = labelService.getLabel("pn2_6_0", LabelContentType.PDF, UUID.randomUUID());
                    response.setHeader("Content-Length", String.valueOf(file.length()));
                    response.setHeader("Content-Disposition", "attachment; filename=\"labels.pdf\"");
                    inputStream = new FileInputStream(file);
                    if (file != null) {
                        IOUtils.copy(inputStream, output);
                    }
                } catch (PeernetException e) {
                    logger.error(e.getMessage(), e);
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                } catch (URISyntaxException e) {
                    logger.error(e.getMessage(), e);
                } catch (ZipException e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
            }
        };
    }

    @GET
    @Timed
    @Path("/applet")
    @Produces({"application/octet-stream"})
    public StreamingOutput getApplet(@Context final HttpServletResponse response) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                FileInputStream inputStream = null;
                try {
                    File file = labelService.getLabel("pn2_6_0", LabelContentType.APPLET, UUID.randomUUID());
                    response.setHeader("Content-Length", String.valueOf(file.length()));
                    inputStream = new FileInputStream(file);
                    if (file != null) {
                        IOUtils.copy(inputStream, output);
                    }
                } catch (PeernetException e) {
                    logger.error(e.getMessage(), e);
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                } catch (URISyntaxException e) {
                    logger.error(e.getMessage(), e);
                } catch (ZipException e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
            }
        };
    }

    @GET
    @Timed
    @Path("/png")
    @Produces({"application/zip"})
    public StreamingOutput getPNG(@Context final HttpServletResponse response) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                FileInputStream inputStream = null;
                try {
                    File file = labelService.getLabel("pn2_6_0", LabelContentType.PNG, UUID.randomUUID());
                    response.setHeader("Content-Length", String.valueOf(file.length()));
                    response.setHeader("Content-Disposition", "attachment; filename=\"labels.zip\"");
                    inputStream = new FileInputStream(file);
                    if (file != null) {
                        IOUtils.copy(inputStream, output);
                    }
                } catch (PeernetException e) {
                    logger.error(e.getMessage(), e);
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                } catch (URISyntaxException e) {
                    logger.error(e.getMessage(), e);
                } catch (ZipException e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
            }
        };
    }
}
