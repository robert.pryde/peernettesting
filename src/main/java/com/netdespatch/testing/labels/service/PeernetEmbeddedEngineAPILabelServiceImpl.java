package com.netdespatch.testing.labels.service;

import com.netdespatch.testing.labels.printing.peernet.*;
import com.netdespatch.testing.labels.utils.Zip;
import com.netdespatch.testing.labels.utils.ZipException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author rpryde
 * @since 14/10/2015
 */
public class PeernetEmbeddedEngineAPILabelServiceImpl implements LabelService {
    private static final Logger logger = LoggerFactory.getLogger(PeernetEmbeddedEngineAPILabelServiceImpl.class);
    private String applicationRootDir;
    private String serialNumber;

    public PeernetEmbeddedEngineAPILabelServiceImpl(String applicationRootDir, String serialNumber) {

        this.applicationRootDir = applicationRootDir;
        this.serialNumber = serialNumber;
    }

    @Override
    public File getLabel(String peernetVersion, LabelContentType contentType, UUID txnId) throws PeernetException, URISyntaxException, IOException, ZipException {
        LabelPrinter labelPrinter = new PeernetEmbeddedLabelPrinterImpl(serialNumber, applicationRootDir+"/labels/");
        labelPrinter.setSerialNumber(serialNumber);
        Context context = new Context();
        context.put(Context.CONTENT_TYPE, contentType);
        context.put(Context.APPLICATION_ROOT, applicationRootDir);
        context.put(Context.LABEL_PROJECT_FILE_NAME, peernetVersion + "_testLabel.pnj");
        HashMap parameterMap = new HashMap();
        parameterMap.put("labelFormat", context.get(Context.CONTENT_TYPE));
        parameterMap.put("dpi", "600");
        context.put(Context.PARAMETERS, parameterMap);
        context.put(Context.LABEL_REQUEST_NAME, "testFontLoading");
        ArrayList<LabelData> arrayList = new ArrayList<LabelData>();
        LabelData labelData = new LabelData();
        labelData.setLorum("Lorem ipsum dolor sit amet, consectetur adipiscing elit, \nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        arrayList.add(labelData);
        context.put(Context.LABEL_DATA_ARRAY, arrayList);

        if (!contentType.isImageFormat()) {
            return labelPrinter.printLabels(context);
        } else {
            FileInputStream fileInputStream = new FileInputStream(labelPrinter.printLabels(context));
            File tempZipFile = File.createTempFile("sls" + txnId, ".zip");
            Zip.compressToZip(IOUtils.readLines(fileInputStream), System.getProperty("java.io.tmpdir"), tempZipFile, true);
            return tempZipFile;
        }

    }
}
