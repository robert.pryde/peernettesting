package com.netdespatch.testing.labels.service;

import com.netdespatch.testing.labels.printing.peernet.LabelContentType;
import com.netdespatch.testing.labels.printing.peernet.PeernetException;
import com.netdespatch.testing.labels.utils.ZipException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

/**
 * @author rpryde
 * @since 27/01/2016
 */
public interface LabelService {
    File getLabel(String peernetVersion, LabelContentType contentType, UUID txnId) throws PeernetException, URISyntaxException, IOException, ZipException;
}
