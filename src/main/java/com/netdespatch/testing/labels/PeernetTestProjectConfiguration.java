package com.netdespatch.testing.labels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yammer.dropwizard.config.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author rpryde
 * @since 14/10/2015
 */
public class PeernetTestProjectConfiguration extends Configuration {
    @NotEmpty
    private String serialNumber;
    @NotEmpty
    private String applicationRootDir;

    @JsonProperty
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty
    public String getApplicationRootDir() {
        return applicationRootDir;
    }

    @JsonProperty
    public void setApplicationRootDir(String applicationRootDir) {
        this.applicationRootDir = applicationRootDir;
    }
}
