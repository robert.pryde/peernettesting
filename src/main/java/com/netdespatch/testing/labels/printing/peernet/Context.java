package com.netdespatch.testing.labels.printing.peernet;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author rpryde
 * @since 30/01/2015
 */
public class Context {
    public static final Key<String> LABEL_PROJECT_FILE_NAME = new Key<String>("LABEL_PROJECT_FILE_NAME");
    public static final Key<HashMap> PARAMETERS = new Key<HashMap>("PARAMETERS");
    public static final Key<LabelContentType> CONTENT_TYPE = new Key<LabelContentType>("CONTENT_TYPE");
    public static final Key<List<LabelData>> LABEL_DATA_ARRAY = new Key<List<LabelData>>("LABEL_DATA");
    public static final Key<String> LABEL_REQUEST_NAME = new Key<String>("LABEL_REQUEST_NAME");
    public static final Key<String> APPLICATION_ROOT = new Key<String>("APPLICATION_ROOT");


    private HashMap<String, Object> virtualMemory;

    public Context() {
        virtualMemory = new HashMap<String, Object>();
    }


    /**
     * Method to wipe the virtual Memory
     */
    public void clear() {
        virtualMemory.clear();
    }

    /**
     * Get an object from the map. If none is found for the specified key, <code>null</code> is
     * returned.
     *
     * @param key the key to use to find the value object
     * @return the value object
     */
    public <T> T get(Key<T> key) {
        return (T) virtualMemory.get(key.getKey());
    }

    /**
     * Get an object from the map, or return the <code>defaultValue</code> if none was found.
     *
     * @param key          the key to use to find the value object
     * @param defaultValue the default value to use if no value object was found
     * @return the value object
     */
    public <T> T get(Key<T> key, T defaultValue) {
        T t = (T) virtualMemory.get(key.getKey());
        if (t == null) {
            return defaultValue;
        }
        return t;
    }

    /**
     * Put an object in the map with the specified key and value.
     *
     * @param key   the key to use for the value object
     * @param value the value object
     */
    public <T> void put(Key<T> key, T value) {
        virtualMemory.put(key.getKey(), value);
    }

    /**
     * Remove an object from the map if exists, otherwise do nothing.
     *
     * @param key the key of the value object to remove
     * @since 23
     */
    public <T> void remove(Key<T> key) {
        virtualMemory.remove(key.getKey());
    }

    /**
     * Check whether or not the map contains an entry with the specified key.
     *
     * @param key the key of the value object
     * @return <code>true</code> if the value exists, <code>false</code> otherwise
     */
    public <T> boolean contains(Key<T> key) {
        return virtualMemory.containsKey(key.getKey());
    }


    public String toString() {
        ToStringBuilder sb = new ToStringBuilder(this);
        for (Map.Entry<String, Object> e : virtualMemory.entrySet()) {
            sb.append(e.getKey(), e.getValue());
        }
        return sb.toString();
    }

    /**
     * A typesafe key into the virtual memory. Use these keys with the typesafe methods in
     * {@link Context} to add and remove entries in the virtual memory.
     *
     * @param <T> the type of value associated with the key
     */
    public static class Key<T> {
        private final String key;

        public Key(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
