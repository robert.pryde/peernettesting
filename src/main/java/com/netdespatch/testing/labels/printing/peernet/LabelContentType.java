package com.netdespatch.testing.labels.printing.peernet;

/**
 * @author rpryde
 * @since 30/01/2015
 */
public enum LabelContentType {
    APPLET("PEERNET Print Applet Writer", false),
    PDF("PEERNET Adobe PDF Writer", false),
    PNG("PEERNET PNG Image Writer", true),
    ;

    private String convertToName;
    private boolean imageFormat;

    private LabelContentType(String name, boolean imageFormat) {
        this.convertToName = name;
        this.imageFormat = imageFormat;
    }

    public String getConvertToName() {
        return this.convertToName;
    }

    public boolean isImageFormat() {
        return imageFormat;
    }
}
