package com.netdespatch.testing.labels.printing.peernet;

/**
 * @author rpryde
 * @since 30/01/2015
 */
public class LabelData {
    private String lorum = "Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
    private String EP = "EP";

    public String getLorum() {
        return lorum;
    }

    public void setLorum(String lorum) {
        this.lorum = lorum;
    }

    public String getEP() {
        return EP;
    }

    public void setEP(String EP) {
        this.EP = EP;
    }
}
