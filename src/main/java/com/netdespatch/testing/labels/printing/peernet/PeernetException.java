package com.netdespatch.testing.labels.printing.peernet;

/**
 * @author rpryde
 * @since 30/01/2015
 */
public class PeernetException extends Exception {
    public PeernetException() {
    }

    public PeernetException(String message) {
        super(message);
    }

    public PeernetException(Throwable cause) {
        super(cause);
    }
}
