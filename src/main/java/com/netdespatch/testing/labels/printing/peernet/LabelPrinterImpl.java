package com.netdespatch.testing.labels.printing.peernet;

import com.peernet.report.engine.Project;
import com.peernet.report.engine.ProjectManager;
import com.peernet.report.engine.Request;
import com.peernet.report.engine.RuntimeInterface;
import com.sun.webkit.Timer;
import edu.emory.mathcs.backport.java.util.concurrent.ConcurrentHashMap;
import edu.emory.mathcs.backport.java.util.concurrent.ConcurrentMap;
import edu.emory.mathcs.backport.java.util.concurrent.locks.Lock;
import edu.emory.mathcs.backport.java.util.concurrent.locks.ReadWriteLock;
import edu.emory.mathcs.backport.java.util.concurrent.locks.ReentrantReadWriteLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * This class is based on how NDServe instantiates and run the PEERNET label printing
 */
public class LabelPrinterImpl implements LabelPrinter {

    /**
     * Inner runtime exception class specific to peernet
     */
    private static class PeernetRuntimeException extends RuntimeException {
        private static final long serialVersionUID = 8327470890090073402L;

        public PeernetRuntimeException(String message, Throwable cause) {
            super(message, cause);
        }

        public PeernetRuntimeException(String message) {
            super(message);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(LabelPrinterImpl.class);

    private final RuntimeInterface projectLoadingRuntime;

    private static final String FONT_MANAGER = "Application.font.manager";
    private static final ConcurrentMap projectCache = new ConcurrentHashMap();
    private static final ReadWriteLock fontManagerLock = new ReentrantReadWriteLock();
    private static Object fontManager; // set after the first label request, used by all subsequent requests
    private String serialNumber;

    public LabelPrinterImpl(String rootpath) {
        logger.info("Creating Label printer class");
        projectLoadingRuntime = createRuntime(rootpath, "", "");
    }

    /**
     * method to call to print labels returns the tmp file created containing the label output
     *
     * @param context
     * @return
     * @throws PeernetException
     */
    @Override
    public File printLabels(Context context)
            throws PeernetException, URISyntaxException {
        RuntimeInterface runtime = createRuntime(context.get(Context.APPLICATION_ROOT), "Exception for labelRequest", serialNumber);
        File labelFile = loadLabelProjectFilePath(context);
        Map<String, Object> parameters = context.get(Context.PARAMETERS);
        Project project = loadLabelProject(labelFile, projectLoadingRuntime);
        getFontManagerFromRuntime(projectLoadingRuntime);
        setFontManagerOnRuntime(runtime);

        Request request = loadRequest(context, project);

        return printLabels(request, runtime, buildParameters(context, parameters));
    }

    private File loadLabelProjectFilePath(Context context) throws URISyntaxException {
        return new File(context.get(Context.APPLICATION_ROOT) + "/labels/" + context.get(Context.LABEL_PROJECT_FILE_NAME));
    }

    /**
     * Helper method for building the label parameters
     *
     * @param parameters
     * @return
     */
    private Hashtable<String, Object> buildParameters(Context context,
                                                      Map<String, Object> parameters) {
        Hashtable<String, Object> ht = new Hashtable<String, Object>(parameters);
        ht.put("labelArray", context.get(Context.LABEL_DATA_ARRAY));
        return ht;
    }

    /**
     * Font manager helper
     *
     * @param runtime
     */
    private void setFontManagerOnRuntime(RuntimeInterface runtime) {
        Lock readLock = fontManagerLock.readLock();
        try {
            readLock.lock();
            if (fontManager != null) {
                runtime.putProperty(FONT_MANAGER, fontManager);
            }
        } finally {
            readLock.unlock();
        }
    }

    /**
     * Load font manager from runtime
     *
     * @param runtime
     */
    private void getFontManagerFromRuntime(RuntimeInterface runtime) {
        Lock writeLock = fontManagerLock.writeLock();
        try {
            logger.info("Obtaining write lock for fontManager");
            writeLock.lock();
            if (fontManager == null) {
                fontManager = runtime.getProperty(FONT_MANAGER);
            }
        } finally {
            logger.info("Releasing write lock for fontManager");
            writeLock.unlock();
        }
    }

    /**
     * Generate the tmp label file
     *
     * @param request
     * @param runtime
     * @param parameters
     * @return
     * @throws PeernetException
     */
    private File printLabels(Request request, RuntimeInterface runtime,
                             Hashtable<String, Object> parameters) throws PeernetException {
        OutputStream os = null;
        try {
            File tmp = File.createTempFile("pnreq", ".tmp");
            os = new FileOutputStream(tmp);
            logger.info("Request={}, Runtime={}, parameters={}, outputStream={}",
                    request, runtime, parameters, os);
            long timeStart = Calendar.getInstance().getTimeInMillis();
            request.run(runtime, null, null, parameters, os);
            logger.info("Label File generated fileSize={}, fileName={} timeTaken={}", tmp.getTotalSpace(), tmp.getName(), Calendar.getInstance().getTimeInMillis()-timeStart);
            return tmp;
        } catch (Exception e) {
            throw new PeernetException(e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    throw new PeernetException(e);
                }
            }
        }
    }

    /**
     * Ensure request exists in label project
     *
     * @param project
     * @return
     * @throws PeernetException
     */
    private Request loadRequest(Context context, Project project)
            throws PeernetException {
        Request request = project.cloneRequest(context.get(Context.LABEL_REQUEST_NAME));
        if (request == null) {
            throw new PeernetException("Request invalid " + context.get(Context.LABEL_REQUEST_NAME));
        }
        return request;
    }

    /**
     * Load label project file
     *
     * @param labelFile
     * @param runtime
     * @return
     * @throws PeernetException
     */
    private Project loadLabelProject(File labelFile, RuntimeInterface runtime)
            throws PeernetException {
        PeernetProjectCacheEntry entry = (PeernetProjectCacheEntry) projectCache.get(labelFile.getAbsolutePath());
        if (entry == null) {
            logger.info("Label " + labelFile.getAbsolutePath() + " is not cached");
            entry = createProjectCacheEntry(labelFile, runtime);
            projectCache.putIfAbsent(labelFile.getAbsolutePath(), entry);
        } else if (labelFile.lastModified() > entry.lastmodified) {
            logger.info("Cached label" + labelFile.getAbsolutePath() + " is tainted, reloading");
            entry = createProjectCacheEntry(labelFile, runtime);
            projectCache.put(labelFile.getAbsolutePath(), entry);
        }
        return entry.project;
    }

    /**
     * Cache project
     *
     * @param labelFile
     * @param runtime
     * @return
     * @throws PeernetException
     */
    private PeernetProjectCacheEntry createProjectCacheEntry(File labelFile,
                                                             RuntimeInterface runtime) throws PeernetException {
        Project project;
        try {
            project = ProjectManager.open(runtime, null, labelFile.toURI().toURL());
        } catch (Exception e) {
            throw new PeernetException(e);
        }
        PeernetProjectCacheEntry entry;
        entry = new PeernetProjectCacheEntry(labelFile, project, labelFile.lastModified());
        return entry;
    }

    /**
     * Create runtime
     *
     * @param rootPath
     * @param exceptionInfo
     * @param serialNumber
     * @return
     */
    private static RuntimeInterface createRuntime(final String rootPath, final String exceptionInfo, final String serialNumber) {
        class ErrorHandler implements RuntimeInterface {
            private Map<Object, Object> props = new HashMap<Object, Object>();

            public boolean containsProperty(Object key) {
                return props.containsKey(key);
            }

            public Component getDesktop() {
                return null;
            }

            public Object getProperty(Object key) {
                return props.get(key);
            }

            public void putProperty(Object key, Object value) {
                props.put(key, value);
            }

            public void reportError(Throwable t) {
                throw new PeernetRuntimeException(exceptionInfo, t);
            }

            public void reportError(String message) {
                throw new PeernetRuntimeException(exceptionInfo + " " + message);
            }

            public void reportError(String message, Throwable cause) {
                throw new PeernetRuntimeException(exceptionInfo + " " + message, cause);
            }

            public void reportException(Throwable t) {
                throw new PeernetRuntimeException(exceptionInfo, t);
            }

            public void reportException(String message, Throwable cause) {
                throw new PeernetRuntimeException(exceptionInfo + " " + message, cause);
            }

            public void reportMessage(String message) {
                logger.info(message);
            }
        }

        logger.info("Root path: {}", rootPath);
        RuntimeInterface runtime = new ErrorHandler();
        runtime.putProperty("Application.dir", rootPath);
        runtime.putProperty("Application.license.dir", rootPath + "licenses");
        runtime.putProperty("Application.fonts.dir", rootPath + "fonts");
        runtime.putProperty("Application.license.serialNumber", serialNumber);
        return runtime;
    }

    @Override
    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
