package com.netdespatch.testing.labels.printing.peernet;

import com.peernet.report.engine.Project;

import java.io.File;

/**
 * @author rpryde
 * @since 30/01/2015
 */
public class PeernetProjectCacheEntry {
    public File file;
    public Project project;
    public long lastmodified;

    public PeernetProjectCacheEntry(File f, Project p, long lastmodified) {
        this.file = f;
        this.project = p;
        this.lastmodified = lastmodified;
    }
}
