package com.netdespatch.testing.labels.printing.peernet;

import java.io.File;
import java.net.URISyntaxException;

/**
 * @author rpryde
 * @since 26/01/2016
 */
public interface LabelPrinter {
    File printLabels(Context context) throws PeernetException, URISyntaxException;

    String getSerialNumber();

    void setSerialNumber(String serialNumber);
}
