package com.netdespatch.testing.labels.health;

import com.yammer.metrics.core.HealthCheck;
import org.apache.commons.lang3.StringUtils;

/**
 * @author rpryde
 * @since 14/10/2015
 */
public class PeernetTestProjectHealthCheck extends HealthCheck {
    private String serialNumber;

    public PeernetTestProjectHealthCheck(String serialNumber) {
        super("Serial Number Heath Check");
        this.serialNumber = serialNumber;
    }

    @Override
    protected Result check() throws Exception {
        if (StringUtils.isBlank(serialNumber)) {
            return Result.healthy();
        } else {
            return Result.healthy("missing serial number");
        }
    }
}
