//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.netdespatch.testing.labels.utils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Zip {
    private static final Logger logger = LoggerFactory.getLogger(Zip.class);

    public static File unCompressFile(String zipFileName, boolean deleteInputFile) throws ZipException {
        try {
            File e = new File(zipFileName);
            ZipInputStream zis = new ZipInputStream(new FileInputStream(e));
            File tmpDir = new File(e.getAbsolutePath() + ".dir");
            if(!tmpDir.mkdirs()) {
                logger.debug("failed to create temporary directory " + tmpDir.getAbsolutePath());
                throw new ZipException("Failed to create temp dir - " + tmpDir.getAbsolutePath());
            } else {
                for(ZipEntry entry = zis.getNextEntry(); entry != null; entry = zis.getNextEntry()) {
                    File outputFile = new File(tmpDir, entry.getName());
                    outputFile.getParentFile().mkdirs();
                    FileOutputStream os = new FileOutputStream(outputFile);
                    byte[] buf = new byte[4096];

                    int len;
                    while((len = zis.read(buf)) > 0) {
                        os.write(buf, 0, len);
                    }

                    os.close();
                }

                zis.close();
                if(deleteInputFile) {
                    e.delete();
                }

                return tmpDir;
            }
        } catch (FileNotFoundException var10) {
            logger.debug("--- File " + zipFileName + " not found", var10);
            throw new ZipException("File " + zipFileName + " not found - " + var10.getMessage());
        } catch (IOException var11) {
            logger.debug("IOException reading " + zipFileName, var11);
            throw new ZipException("IOException reading " + zipFileName + " - " + var11.getMessage());
        }
    }

    public static void compressToZip(List<String> sourceFiles, String archiveRootDir, File outputFile, boolean includeIndexFile) throws ZipException {
        logger.debug("Compressing {}", sourceFiles);
        if(sourceFiles != null && sourceFiles.size() != 0) {
            if(outputFile == null) {
                throw new ZipException("outputFile is null");
            } else if(archiveRootDir == null) {
                throw new ZipException("archiveRootDir is null");
            } else {
                if(!archiveRootDir.endsWith(File.separator)) {
                    archiveRootDir = archiveRootDir + File.separatorChar;
                }

                ZipOutputStream zipFile = null;
                FileOutputStream indexFileOS = null;
                File indexFile = null;

                try {
                    ArrayList e = null;
                    if(includeIndexFile) {
                        e = new ArrayList();
                    }

                    zipFile = new ZipOutputStream(new FileOutputStream(outputFile));
                    Iterator i$ = sourceFiles.iterator();

                    String indexEntry;
                    while(i$.hasNext()) {
                        indexEntry = (String)i$.next();
                        compressToZip(indexEntry, archiveRootDir, zipFile, e);
                    }

                    if(e != null) {
                        indexFile = File.createTempFile("index", ".txt");
                        indexFileOS = new FileOutputStream(indexFile);
                        i$ = e.iterator();

                        while(i$.hasNext()) {
                            indexEntry = (String)i$.next();
                            IOUtils.write(indexEntry + "\r\n", indexFileOS);
                        }

                        compressToZip(indexFile.getPath(), indexFile.getParent() + File.separatorChar, zipFile, (List)null);
                    }
                } catch (FileNotFoundException var15) {
                    logger.error(var15.getMessage(), var15);
                    throw new ZipException("File " + outputFile + " not found - " + var15.getMessage());
                } catch (IOException var16) {
                    logger.error(var16.getMessage(), var16);
                    throw new ZipException("Unable to create zip index file - " + var16.getMessage());
                } finally {
                    IOUtils.closeQuietly(zipFile);
                    IOUtils.closeQuietly(indexFileOS);
                    if(indexFile != null && indexFile.exists()) {
                        indexFile.delete();
                    }

                }

                logger.debug("Compression finished {}", outputFile);
            }
        } else {
            throw new ZipException("File list is null or empty");
        }
    }

    private static void compressToZip(String sourceFile, String archiveRootDir, ZipOutputStream out, List<String> indexEntries) throws ZipException {
        try {
            compressToOutputStreamRecursively(archiveRootDir, sourceFile, out, indexEntries);
        } catch (IOException var5) {
            logger.error(var5.getMessage(), var5);
            throw new ZipException("IOException archiving " + sourceFile + " - " + var5.getMessage());
        }
    }

    private static void compressToOutputStreamRecursively(String rootDir, String sourceDir, ZipOutputStream out, List<String> indexEntries) throws IOException, ZipException {
        if(sourceDir != null) {
            File sourceDirFile = new File(sourceDir);
            if(sourceDirFile.isFile()) {
                compressFileToZip(rootDir, out, sourceDirFile, indexEntries);
            } else {
                if(!sourceDirFile.isDirectory()) {
                    throw new ZipException("Not a file or directory: " + sourceDir);
                }

                File[] files = sourceDirFile.listFiles();
                if(files != null) {
                    File[] arr$ = files;
                    int len$ = files.length;

                    for(int i$ = 0; i$ < len$; ++i$) {
                        File file = arr$[i$];
                        compressToOutputStreamRecursively(rootDir, file.getPath(), out, indexEntries);
                    }
                }
            }
        }

    }

    private static void compressFileToZip(String rootDir, ZipOutputStream out, File file, List<String> indexEntries) throws IOException {
        FileInputStream in = null;

        try {
            String entryPath = file.getPath().replace(rootDir, "");
            ZipEntry entry = new ZipEntry(entryPath);
            out.putNextEntry(entry);
            if(indexEntries != null) {
                indexEntries.add(entryPath);
            }

            in = new FileInputStream(file);
            IOUtils.copy(in, out);
            IOUtils.closeQuietly(in);
        } finally {
            IOUtils.closeQuietly(in);
        }

    }
}
