package com.netdespatch.testing.labels;

import com.netdespatch.testing.labels.health.PeernetTestProjectHealthCheck;
import com.netdespatch.testing.labels.resource.LabelResource;
import com.netdespatch.testing.labels.resource.PeernetEmbeddedEngineAPILabelResource;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.assets.AssetsBundle;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;

/**
 * @author rpryde
 * @since 14/10/2015
 */
public class PeernetTestProjectApplication extends Service<PeernetTestProjectConfiguration> {
    public static void main(String[] args) throws Exception {
        new PeernetTestProjectApplication().run(args);
    }

    @Override
    public void run(PeernetTestProjectConfiguration configuration, Environment environment) throws Exception {
        final LabelResource labelResource = new LabelResource(configuration.getApplicationRootDir(), configuration.getSerialNumber());
        environment.addResource(labelResource);

        final PeernetEmbeddedEngineAPILabelResource engineAPILabelResource = new PeernetEmbeddedEngineAPILabelResource(configuration.getApplicationRootDir(), configuration.getSerialNumber());
        environment.addResource(engineAPILabelResource);

        final PeernetTestProjectHealthCheck healthCheck = new PeernetTestProjectHealthCheck(configuration.getSerialNumber());
        environment.addHealthCheck(healthCheck);

    }

    @Override
    public void initialize(Bootstrap<PeernetTestProjectConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/webapp", "/"));
    }
}
