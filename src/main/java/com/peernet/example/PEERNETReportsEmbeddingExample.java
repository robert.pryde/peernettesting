package com.peernet.example;

import com.peernet.report.embedded.Engine;
import com.peernet.report.embedded.Request;

import java.io.File;
import java.io.IOException;

import java.io.FileOutputStream;

/**
 * @author 000145147
 * @version $Revision: 1.17 $
 */
public class PEERNETReportsEmbeddingExample {


    static public void main(String argv[]) {


        // Step 1 : Create a instance of the embedded engine
        String serialNumber = ""; // Serial Number for In-House or Distribution license
        String subscriptionCode = ""; // Subscription Code for In-House license obtained from the Help/About dialog in PEERNET Reports when licensed using In-House serial number.
        File projectFolder = new File("C:\\Program Files (x86)\\PEERNETReports3.0.093\\javaexamples\\PEERNETReportsSampleEmbedding\\forms"); // Folder contain the projects to be loaded so we can use relative path for loading projects.

        //
        // IMPORTANT: passing empty strings will run engine in 30-day evaluation mode
        // and all output generate will be marked with an big X and attribution information to
        // the fact that it is running in evaluation mode
        //
        Engine engine = new Engine(serialNumber, subscriptionCode, projectFolder.getAbsolutePath());

        try {

            // Step 2 : use a request to perform all embedded operations

            // NOTE: In this sample there are three different requests to demostrate some of the basics of reuse


            FileOutputStream out = null;

            // Example 1 - Start

            try {

                // Step 2 : use a request to perform all embedded operations
                //   Here we are loading a request to create a ZPL print file which we would send to a Zebra Thermal printer
                //


                // Step: 2a - open up a request
                Request request = engine.openRequest("sample.pnj", "RequestZebraZPL", true, true);


                // Step: 2b - setup where you want the resultant stream to be stored
                // We want to save the ZPL print stream to a disk file so we create a FileOutputStream.
                // NOTE: You could save in memory using a ByteArrayOutputStream, we are just showing how to save to disk file in this sample.
                out = new FileOutputStream("C:\\Users\\rpryde\\printing" +
                        File.separator + "4x6label-200dpi.zpl");

                // Step: 2c - setup your parameters
                request.clearAllParameters(); // This is just to ensure we are starting with no paremeters set.

                // Step: 2d - setup your parameters
                // If there are any parameters needed by this request or any of the sources used by the request (reports or labels) this
                // is were we set them to be passed.
                //
                // NOTE: In this sample we are creating a basic 4x6 label with the company name and barcode data being parameters of the reference label
                //
                request.setParameter("CompanyName", "PEERNET Inc.");
                request.setParameter("BarcodeData", "1234567890");


                // Step 2e - make any other changes you wish to control other aspects of the generated result
                com.peernet.report.engine.ConvertTo convertTo = request.getRequest().getConvertTo("PEERNET ZEBRA Thermal Printer");
                if (convertTo != null) {
                    // Here we can change the resolution to match the Zebra printer we are targetting to print on.  200, 300, or 600 dpi
                    convertTo.setSetting("PrintQuality", new Integer(200));
                    convertTo.setSetting("Resolution", new Integer(200));
                }

                // Step 2f - run the request to generate the resultant stream
                request.execute(out, null);

            } catch (Throwable ex) {
                ex.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            // Example 1 - End

            // Example 2 - Start

            try {

                // Step 2 : use a request to perform all embedded operations
                //   Here we are loading a request to create a PNG image file which you can open to see what would have printed
                //   on the Zebra printer if you could send the Zebra Print File to a 200 dpi zebra printer.
                //


                // Step: 2a - open up a request
                Request request = engine.openRequest("sample.pnj", "RequestPNGImage", true, true);


                // Step: 2b - setup where you want the resultant stream to be stored
                // NOTE: You could save in memory using a ByteArrayOutputStream, we are just showing how to save to disk file in this sample.
                out = new FileOutputStream("C:\\Users\\rpryde\\printing" +
                        File.separator + "4x6label-200dpi.png");

                // Step: 2c - setup your parameters
                request.clearAllParameters(); // This is just to ensure we are starting with no paremeters set.

                // Step: 2d - setup your parameters
                // If there are any parameters needed by this request or any of the sources used by the request (reports or labels) this
                // is were we set them to be passed.
                //
                // NOTE: In this sample we are creating a basic 4x6 label with the company name and barcode data being parameters of the reference label
                //
                request.setParameter("CompanyName", "PEERNET Inc.");
                request.setParameter("BarcodeData", "1234567890");


                // Step 2e - make any other changes you wish to control other aspects of the generated result
                com.peernet.report.engine.ConvertTo convertTo = request.getRequest().getConvertTo("PEERNET PNG Image Writer");
                if (convertTo != null) {
                    // Here we can change the resolution so the PNG Image file comes out at 200 dpi
                    convertTo.setSetting("PrintQuality", new Integer(200));
                    convertTo.setSetting("Resolution", new Integer(200));
                }

                // Step 2f - run the request to generate the resultant stream
                request.execute(out, null);

            } catch (Throwable ex) {
                ex.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            // Example 2 - End

            // Example 3 - Start

            try {

                // Step 2 : use a request to perform all embedded operations
                //   Here we are loading a request to create a Adobe PDF file of the 4x6 inch label
                //


                // Step: 2a - open up a request
                Request request = engine.openRequest("sample.pnj", "RequestPDFDocument", true, true);


                // Step: 2b - setup where you want the resultant stream to be stored
                // NOTE: You could save in memory using a ByteArrayOutputStream, we are just showing how to save to disk file in this sample.
                out = new FileOutputStream("C:\\Users\\rpryde\\printing" +
                        File.separator + "4x6label.pdf");

                // Step: 2c - setup your parameters
                request.clearAllParameters(); // This is just to ensure we are starting with no paremeters set.

                // Step: 2d - setup your parameters
                // If there are any parameters needed by this request or any of the sources used by the request (reports or labels) this
                // is were we set them to be passed.
                //
                // NOTE: In this sample we are creating a basic 4x6 label with the company name and barcode data being parameters of the reference label
                //
                request.setParameter("CompanyName", "PEERNET Inc.");
                request.setParameter("BarcodeData", "1234567890");

                // Step 2e - run the request to generate the resultant stream
                request.execute(out, null);

            } catch (Throwable ex) {
                ex.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            // Example 3 - End

            // Example 4 - Start

            try {

                // Step 2 : use a request to perform all embedded operations
                //   Here we are loading a request to create a Adobe PDF
                //
                //  The purpose of this example is to demostrate how to use a Java Object table(s)
                //  to pass information to the reporting engine.  This allows the data to be obtained from
                //  anywhere.
                //
                //  To keep this not to complex, the sample uses a single Java Object Table named Labels
                //     The table Labels has 3 columns:
                //           Column 1: 'PartNumber' String
                //           Column 2: 'PartDescription' String
                //


                // Step: 2a - open up a request
                Request request = engine.openRequest("sample.pnj", "RequestLabels", true, true);


                // Step: 2b - setup where you want the resultant stream to be stored
                // NOTE: You could save in memory using a ByteArrayOutputStream, we are just showing how to save to disk file in this sample.
                out = new FileOutputStream("C:\\Users\\rpryde\\printing" +
                        File.separator + "Labels.pdf");

                // Step: 2c - setup your parameters
                request.clearAllParameters(); // This is just to ensure we are starting with no paremeters set.

                // Step: 2d - setup your parameters
                // If there are any parameters needed by this request or any of the sources used by the request (reports or labels) this
                // is were we set them to be passed.


                request.setParameter("CompanyName", "PEERNET Inc.");

                //
                // NOTE: In this sample we are creating serials of labels using a Java Object Table named Labels
                // which will be passed as a parameter to the request and used to drive the creation of the labels
                //
                // A table is a collection of rows and this is represented by a Vector of Vectors.

                String[][] rows = {
                        {"1200001", "Pin"},
                        {"1400212", "Nut"},
                        {"1241231", "Bolt"}
                };

                java.util.Vector tableRows = new java.util.Vector();
                for (int index = 0; index < rows.length; index++) {
                    String[] row = rows[index];
                    java.util.Vector tableRow = new java.util.Vector();
                    tableRow.add(row[0]); // PartNumber
                    tableRow.add(row[1]); // PartDescription
                    tableRows.add(tableRow);
                }

                request.setParameter("Labels", tableRows); // This is one way to pass the table contents


                // Step 2e - run the request to generate the resultant stream
                request.execute(out, null);

            } catch (Throwable ex) {
                ex.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            // Example 4 - End


        } catch (Throwable ex) {

            ex.printStackTrace();
        }

        System.exit(0);
    }

}
