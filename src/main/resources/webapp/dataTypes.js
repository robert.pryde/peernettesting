
function getDataTypeHelp(dataType) {
	var fmtStr;
	fmtStr="";
	if(dataType=="DateTime") {
		fmtStr+="<br><u>Date Time formatting symbol guide</u><br>";
		fmtStr+="y - Year (2005, 05)<br>";
		fmtStr+="M - Month in Year (July,Jul,07)<br>";
		fmtStr+="d - Day in Month (10)<br>";
		fmtStr+="H - Hour in Day (24 Hr - 0 to 23)<br>";
		fmtStr+="h - Hour in Day (12 Hr - 1 to 12)<br>";
		fmtStr+="m - Minutes in Hour (45)<br>";
		fmtStr+="S - Seconds in Minute (55)<br>";
		fmtStr+="a - am/pm marker (PM)<br><br>";
		fmtStr+="<u>Examples</u><br>";
		fmtStr+="yyyy-MM-dd HH:mm:SS (2004-21-03 11:33:03)<br>";
		fmtStr+="d/M/yyyy (21/3/2004)<br><br>";
	}
	return fmtStr;
			
}

	var	ie=document.all;
	var	dom=document.getElementById;
	var	ns4=document.layers;
    var lastHBID;
	
	function getObj(objName) {
		var obj=(dom)?document.getElementById(objName) : ie?eval("document.all."+objName) :eval("document."+objName);
		return obj;
	}
	function getObjStyle(objName) {
		var obj=(dom)?document.getElementById(objName).style : ie?eval("document.all."+objName) :eval("document."+objName);
		return obj;
	}

	function closeHelp() {
		var helpBox=getObjStyle("helpBox");
		helpBox.visibility="hidden";
		showElement('SELECT');
		return false;
	}
	function closeGMap() {
		var gmapBox=getObjStyle("gmapBox");
		gmapBox.visibility="hidden";
		showElement('SELECT');
		return false;
	}
	function bkShowGMap(ctl, baseURL, titleStr, address, formatData, leftAdjust, topAdjust, width, height) {

		var gmapBox=getObjStyle("gmapBox");
		if (gmapBox.visibility=="visible" && lastHBID==ctl) {
			return closeGMap();
			return false;
		} else {
			closeGMap();
		}
		lastHBID=ctl;
		var gmapTable=(dom)?document.getElementById("gmapTable") : ie? document.all.gmapTable : document.gmapTable;
		gmapTable.innerHTML="<b>&nbsp;"+titleStr+"</b>";

		var gmapDetail=getObj("gmapDetail");
		var gds = getObjStyle("gmapDetail");
		gds.width=width+"px";
		gds.height=height+"px";
		
		var gMapURL=baseURL+"/gmap/index.php?fy="+escape("UK")+"&fz="+escape(address)+"&width="+width+"&height="+height;
		gmapDetail.innerHTML="<iframe frameborder=\"0\" scrolling=\"no\" width=\"100%\" height=\"100%\" src=\""+gMapURL+"\"></iframe>";

		var toppos=getTopPos(ctl);
		var leftpos=getLeftPos(ctl);

		var newTopPos = ctl.offsetTop+ctl.offsetHeight+toppos-25+topAdjust;
		var newLeftPos = ctl.offsetLeft+ctl.offsetWidth+leftpos+leftAdjust;
		if(newTopPos<10) newTopPos=10;
		gmapBox.top = newTopPos;
		gmapBox.left= newLeftPos;
		
		hideElement('SELECT', document.getElementById("gmapBox"));
		gmapBox.visibility="visible";
		return false;
	}
	
	function showHelp(ctl, titleStr, description, formatData, dataType, leftAdjust, topAdjust, topMin) {
		if(leftAdjust==null)
			leftAdjust=0;
		if(topAdjust==null)
			topAdjust=0;
		var helpBox=getObjStyle("helpBox");
		if (helpBox.visibility=="visible" && lastHBID==ctl) {
			return closeHelp();
			return false;
		} else {
			closeHelp();
		}
		lastHBID=ctl;
		var helpTable=(dom)?document.getElementById("helpTable") : ie? document.all.helpTable : document.helpTable;
		helpTable.innerHTML="<b>&nbsp;"+titleStr+"</b>";

		var helpDetail=getObj("helpDetail");
		helpDetail.innerHTML="<span class=\"small\"><u>Description</u><br>"+description+"</span>";

		var helpFormat=getObj("helpFormat");
		var fmtStr = "<span class=\"small\">";
		if(formatData!="") {
			fmtStr+="<br><u>Format</u><br><b>"+formatData+"</b><br>";
		}
		fmtStr+=getDataTypeHelp(dataType);
		fmtStr+="</span>";
		helpFormat.innerHTML=fmtStr;
		var helpBtm=getObj("helpBtm");
		var toppos=getTopPos(ctl);
		var leftpos=getLeftPos(ctl);

		var newTopPos = ctl.offsetTop+ctl.offsetHeight+toppos-(helpBtm.offsetTop/1.5)+topAdjust;
		var newLeftPos = ctl.offsetLeft+ctl.offsetWidth+leftpos+leftAdjust;
		if(topMin<=0)
			topMin=110;
		if(newTopPos<topMin) newTopPos=topMin;
		helpBox.top = newTopPos;
		helpBox.left= newLeftPos;
		
		hideElement('SELECT', document.getElementById("helpBox"));
		helpBox.visibility="visible";
		return false;
	}
	function getTopPos(ctl) {
		var	leftpos=0;
		var	toppos=0;
		aTag = ctl;
		do {
			aTag = aTag.offsetParent;
			leftpos	+= aTag.offsetLeft;
			toppos += aTag.offsetTop;
		} while(aTag.tagName!="BODY");
		return toppos;
	
	}
	function getLeftPos(ctl) {
		var	leftpos=0;
		var	toppos=0;
		aTag = ctl;
		do {
			aTag = aTag.offsetParent;
			leftpos	+= aTag.offsetLeft;
			toppos += aTag.offsetTop;
		} while(aTag.tagName!="BODY");
		return leftpos;
	
	}	

