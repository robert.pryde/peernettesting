# Set JAVA_HOME
Use the same version of java as the project was compiled in (or later) - requires java1.7+
linux

    JAVA_HOME=<path to jdk/jre>
    echo $JAVA_HOME

windows

    set JAVA_HOME=<path to jdk/jre>
    echo %JAVA_HOME%

# Modify properties

Populate the serial number property with the correct peernet serial number for the version of peernet being used

    serialNumber:

# Start the server

start the program with the following command:

linux

    ./bin/peernetLabelIssueReplication server peernetTestProjectApplication.yml

windows

    .\bin\peernetLabelIssueReplication server peernetTestProjectApplication.yml

Note that the root folder location on windows may need to be moved to cope with windows file path length issues


# Use

Navigate to port 8080 on machine the server has been started on.

    e.g. http://localhost:8080/index.html